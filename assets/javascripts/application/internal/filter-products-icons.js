$(document).on('ready', function () {
  /* When clicking on one of the products labels in the filter */
  $('.js-filter-product-icons').on('click', function () {
    const filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterProductsOut(filterName)
  })
  $('.js-filter-version-icons').on('click', function () {
    const filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterVersionsOut(filterName)
  })
  $('.js-filter-reported-icons').on('click', function () {
    const filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterReportedOut(filterName)
  })
  countIconsByProducts()
  countIconsByVersion()
})

const countIconsByVersion = () => {
  const $allProducts = $('.js-count-version-icons')
  $.each($allProducts, function (i, v) {
    const productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    const $counterContainer = $(v)
    const $iconsContainer = $(`.icons-list article .js-filter-version-label:contains(${productName})`)
    const countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  })
}

const countIconsByProducts = () => {
  const $allProducts = $('.js-count-product-icons')
  $.each($allProducts, function (i, v) {
    const productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    const $counterContainer = $(v)
    const $iconsContainer = $(`.icons-list article .js-filter-label:contains(${productName})`)
    const countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  })
}

const filterReportedOut = (label) => {
  const $iconsContainer = $('.icons-list article')
  for (let i = 0; i < $iconsContainer.length; i++) {
    const filterLabel = $($iconsContainer[i]).find('.js-filter-reported-label').text()
    if (filterLabel === label || label === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}

const filterProductsOut = (label) => {
  const $iconsContainer = $('.icons-list article')
  for (let i = 0; i < $iconsContainer.length; i++) {
    const filterLabel = $($iconsContainer[i]).find('.js-filter-label').text()
    if (filterLabel === label || label === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}

const filterVersionsOut = (label) => {
  const $iconsContainer = $('.icons-list article')
  for (let i = 0; i < $iconsContainer.length; i++) {
    const filterLabel = $($iconsContainer[i]).find('.js-filter-version-label').text()
    if (filterLabel === label || label === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}
