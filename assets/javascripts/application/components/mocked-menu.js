/* Mocked menu
========================================================================== */
$(function () {
  // mm- prefix stands for 'mocked menu'
  function checkSidebarState () {
    if (window.localStorage.getItem('collapsedSidebar') === 'true') {
      $(this).find('.js-mm-sub').toggleClass('js-mocked-sub-toggle')
    }
  }

  const toggleCollapsed = () => {
    $('.js-mocked-menu').find('.collapse.in').collapse('hide')
  }

  // if we're on the collapsed state, enable hover over colapsible submenu
  $('.js-mm-expandable').hover(checkSidebarState)

  // reset collapsibles when toggling sidebar
  $(document).on('click', '.js-sidebar-toggle', toggleCollapsed)
  // reset collapsibles when clicking on them to avoid overlapped submenus in collapsed state
  $(document).on('click', '.js-mm-title', toggleCollapsed)
})
