const express = require('express')
const router = express.Router()
const fetch = require('node-fetch')
/* eslint-disable */
router.get('/', (req, res, next) => {
  (async () => {
    try {
      const gitlabReq = await fetch('https://gitlab.com/api/v4/projects/3276752/repository/tags')
      const gitRes = await gitlabReq.json()
      res.status(200).send(gitRes)
    } catch (err) {
      console.log(err)
    }
  })()
})

module.exports = router
