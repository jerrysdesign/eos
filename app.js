const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const robots = require('express-robots-txt')
const projectConfig = require('./modules/config-middleware')

/* =============================
  Routes files declarations
============================= */
// index pages
const landingPage = require('./routes/landing-page/index')
const icons = require('./routes/icons/index')
const dashboard = require('./routes/dashboard/index')
const brandColors = require('./routes/colors/index')
const dashboardWriting = require('./routes/writing/index')
const cookiesPolicy = require('./routes/shared/index')
const changeLog = require('./routes/changelog/index')
const buttons = require('./routes/buttons/index')
const typography = require('./routes/typography/index')
const alerts = require('./routes/alerts/index')
// submenu pages
const howTo = require('./routes/colors/how-to')
const eosIconsSet = require('./routes/icons/eos-icons-set')
const iconsWhenToUseThem = require('./routes/icons/when-to-use-them')
const logoIconsSet = require('./routes/icons/logo-icons-set')
const uxWriting = require('./routes/writing/ux-writing')
const brandVoice = require('./routes/writing/brand-voice')
const brandTone = require('./routes/writing/brand-tone')
const conventionsAndRules = require('./routes/writing/conventions-and-rules')
const iconsUx = require('./routes/icons/icon-ux')
const iconButtons = require('./routes/buttons/icon-buttons')
const buttonsSizing = require('./routes/buttons/sizing')
const colorsGradient = require('./routes/colors/gradients')
const alertsGlobal = require('./routes/alerts/global')
const alertsSection = require('./routes/alerts/section')
const alertsInline = require('./routes/alerts/inline')

// news
const news = require('./routes/news/index')
// Internal pages
const iconsReport = require('./routes/internal/icons-report')
const specialIcons = require('./routes/internal/special-icons')
const spacewalkIconsSuma = require('./routes/internal/spacewalk-icons-suma')
const spacewalkIconsSCC = require('./routes/internal/spacewalk-icons-scc')
const stratosIcons = require('./routes/internal/stratos-icons')
const designSpecs = require('./routes/internal/design-specs')
const mockedMenu = require('./routes/internal/mocked-menu')

// API
const iconsAPI = require('./routes/api/icons/index')
const gitlabAPI = require('./routes/api/gitlab/index')
const feedbackTool = require('./routes/api/feedback-tool/index')
const logoIcons = require('./routes/api/logo-icons/index')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(favicon(path.join(__dirname, 'assets/images', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'assets')))
app.use(express.static(path.join(__dirname, 'vendors')))
app.use(express.static(path.join(__dirname, 'vendors-landing')))
app.use(projectConfig)

/* Set public folder for sitemaps, pdf etc.. */
app.use(express.static(path.join(__dirname, 'public')))
/* Set robots.txt with express middleware, for the sitemap, place it inside /public folder */
if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === undefined) {
  /* Disallow bots for development/staging */
  app.use(robots({UserAgent: '*', Disallow: '/'}))
}
if (process.env.NODE_ENV === 'production') {
  /* Allow bots for production */
  app.use(robots({UserAgent: '*', Disallow: ['/cookies-policy'], Sitemap: '/sitemap.xml'}))
}

/* =============================
  Routes definitions
============================= */
// index pages routes
app.use('/', landingPage)
app.use('/dashboard', dashboard)
app.use('/icons', icons)
app.use('/colors', brandColors)
app.use('/writing', dashboardWriting)
app.use('/cookies-policy', cookiesPolicy)
app.use('/changelog', changeLog)
app.use('/buttons', buttons)
app.use('/typography', typography)
app.use('/alerts', alerts)
// submenu routes
app.use('/colors/how-to', howTo)
app.use('/colors/gradients', colorsGradient)
app.use('/icons/when-to-use-them', iconsWhenToUseThem)
app.use('/icons/eos-icons-set', eosIconsSet)
app.use('/icons/icon-ux', iconsUx)
app.use('/icons/logo-icons-set', logoIconsSet)
app.use('/writing/ux-writing', uxWriting)
app.use('/writing/brand-voice', brandVoice)
app.use('/writing/brand-tone', brandTone)
app.use('/writing/conventions-and-rules', conventionsAndRules)
app.use('/internal/special-icons', specialIcons)
app.use('/internal/design-specs', designSpecs)
app.use('/internal/stratos-icons', stratosIcons)
app.use('/internal/icons-report', iconsReport)
app.use('/internal/spacewalk-icons-suma', spacewalkIconsSuma)
app.use('/internal/spacewalk-icons-scc', spacewalkIconsSCC)
app.use('/internal/mocked-menu', mockedMenu)
app.use('/buttons/icon-buttons', iconButtons)
app.use('/buttons/sizing', buttonsSizing)
app.use('/alerts/global', alertsGlobal)
app.use('/alerts/section', alertsSection)
app.use('/alerts/inline', alertsInline)

// news routes
app.use('/news', news)
// API
app.use('/api/icons', iconsAPI)
app.use('/api/gitlab', gitlabAPI)
app.use('/api/feedback', feedbackTool)
app.use('/api/logo-icons', logoIcons)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).render('error/404/index')
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
